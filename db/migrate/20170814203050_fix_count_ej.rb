class FixCountEj < ActiveRecord::Migration[5.1]
  def change
    remove_column :ejs, :count, :integer
    
    add_column :ejs, :count, :integer, :default => 0
  end
end
