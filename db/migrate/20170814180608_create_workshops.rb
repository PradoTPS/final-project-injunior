class CreateWorkshops < ActiveRecord::Migration[5.1]
  def change
    create_table :workshops do |t|
      t.string :name
      t.datetime :start
      t.datetime :finish

      t.timestamps
    end
  end
end
