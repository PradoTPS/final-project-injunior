class AddProfilePhotoToHotel < ActiveRecord::Migration[5.1]
  def change
    add_column :hotels, :profile_photo, :string
  end
end
