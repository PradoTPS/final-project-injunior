class AddContentToHotel < ActiveRecord::Migration[5.1]
  def change
    add_column :hotels, :content, :string
  end
end
