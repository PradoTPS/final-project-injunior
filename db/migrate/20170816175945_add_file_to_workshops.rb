class AddFileToWorkshops < ActiveRecord::Migration[5.1]
  def change
    add_column :workshops, :file, :string
  end
end
