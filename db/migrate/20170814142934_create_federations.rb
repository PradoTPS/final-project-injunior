class CreateFederations < ActiveRecord::Migration[5.1]
  def change
    create_table :federations do |t|
      t.string :name
      t.string :uf

      t.timestamps
    end
  end
end
