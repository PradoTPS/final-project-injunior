class AddSpeakerToWorkshop < ActiveRecord::Migration[5.1]
  def change
    add_reference :workshops, :speaker, foreign_key: true
  end
end
