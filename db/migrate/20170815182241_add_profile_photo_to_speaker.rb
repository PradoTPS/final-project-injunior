class AddProfilePhotoToSpeaker < ActiveRecord::Migration[5.1]
  def change
    add_column :speakers, :profile_photo, :string
  end
end
