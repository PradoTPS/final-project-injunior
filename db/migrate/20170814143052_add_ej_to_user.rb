class AddEjToUser < ActiveRecord::Migration[5.1]
  def change
    add_reference :users, :ej, foreign_key: true
  end
end
