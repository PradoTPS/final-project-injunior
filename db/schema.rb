# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170816182446) do

  create_table "ejs", force: :cascade do |t|
    t.string "name"
    t.integer "federation_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "count", default: 0
    t.index ["federation_id"], name: "index_ejs_on_federation_id"
  end

  create_table "federations", force: :cascade do |t|
    t.string "name"
    t.string "uf"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "hotels", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "profile_photo"
    t.string "content"
  end

  create_table "lectures", force: :cascade do |t|
    t.string "name"
    t.string "speaker"
    t.string "download"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "rooms", force: :cascade do |t|
    t.integer "num"
    t.integer "capacity"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "hotel_id"
    t.index ["hotel_id"], name: "index_rooms_on_hotel_id"
  end

  create_table "speakers", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "profile_photo"
  end

  create_table "user_inscription_workshops", force: :cascade do |t|
    t.integer "user_id"
    t.integer "workshop_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_user_inscription_workshops_on_user_id"
    t.index ["workshop_id"], name: "index_user_inscription_workshops_on_workshop_id"
  end

  create_table "user_transfer_users", force: :cascade do |t|
    t.integer "user1"
    t.integer "user2"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.string "telephone"
    t.string "password_digest"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "admin"
    t.string "profile_photo"
    t.integer "ej_id"
    t.integer "room_id"
    t.index ["ej_id"], name: "index_users_on_ej_id"
    t.index ["room_id"], name: "index_users_on_room_id"
  end

  create_table "workshops", force: :cascade do |t|
    t.string "name"
    t.datetime "start"
    t.datetime "finish"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "speaker_id"
    t.string "file"
    t.index ["speaker_id"], name: "index_workshops_on_speaker_id"
  end

end
