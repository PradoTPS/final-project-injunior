json.extract! workshop, :id, :name, :start, :finish, :created_at, :updated_at
json.url workshop_url(workshop, format: :json)
