json.extract! federation, :id, :name, :uf, :created_at, :updated_at
json.url federation_url(federation, format: :json)
