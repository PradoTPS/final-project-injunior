json.extract! ej, :id, :name, :federation_id, :created_at, :updated_at
json.url ej_url(ej, format: :json)
