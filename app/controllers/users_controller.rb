class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy, :set_local, :register, :set_register, :workshops, :transfer, :set_transfer]
  before_action :correct_user, only: [:edit, :update]
  before_action :correct_user_or_admin, only: [:destroy]
  before_action :logged_user, only: [:new, :create]
  before_action :non_logged_user, except: [:new, :create, :password_recovery, :set_new_password]
  before_action :set_ejs, only: [:new, :create, :edit]
  before_action :set_rooms, only: [:local]
  before_action :full, only: [:set_local]
  before_action :only_admin, only: [:index]
  
  # GET /users
  # GET /users.json
  def index
    @users = User.all
    @users = User.paginate(:page => params[:page], :per_page => 5)
    
    respond_to do |format|
      format.html
      format.xls
    end
  end

  # GET /users/1
  # GET /users/1.json
  def show
    @ejs_ordered = Ej.order(count: :desc)
    @p = 1
    
    @total = {}
    @current_total = 0
    @pp = 0
    
    Federation.all.each do |federation|
      
      federation.ejs.each do |ej|
        @current_total = @current_total + ej.count
      end
      
      @total = @total.merge({federation.name => @current_total})
      @current_total = 0
      @pp = @pp + 1
    end
    
    @total = @total.sort_by {|key, value| value }.reverse
    @pp = 1
  end

  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit
  end

  # POST /users
  # POST /users.json
  def create
    if User.find_by(:email => user_params[:email])
      redirect_to login_path
      return
    end
    
    @user = User.new(user_params)
    
    @ej = @user.ej
    @ej.count = @ej.count + 1
    @ej.save

    respond_to do |format|
      if @user.save
          UserMailer.confirmation(@user).deliver
          format.html { redirect_to @user, notice: 'User was successfully created.' }
          format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    ej = Ej.find(@user.ej_id)
    
    ej.count = ej.count - 1
    ej.save
    
    inscriptions = UserInscriptionWorkshop.where(:user_id => @user.id)
    
    inscriptions.each do |inscription|
      inscription.destroy
    end
    
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  def local
    @hotels = Hotel.all
  end
  
  def set_local
    @user.update_attributes(room_id: params[:room_id])
  
    redirect_to @user
  end
  
  def register
    @workshops = Workshop.all
    @inscriptions = UserInscriptionWorkshop.where(:user_id => @user.id) 
  end
  
  def set_register
    if params[:att][:action] == "do"
      inscription = UserInscriptionWorkshop.new
      inscription.user_id = @user.id
      inscription.workshop_id = params[:att][:workshop_id]
      inscription.save
     
    elsif params[:att][:action] == "undo"
      inscription = UserInscriptionWorkshop.find_by(user_id: @user.id, workshop_id: params[:att][:workshop_id])
      inscription.destroy
    end
    @workshop = Workshop.find(params[:att][:workshop_id])
    respond_to do |format|
      format.html{redirect_to register_path(@user)}
      format.js{}
    end
  end
  
  def transfer
  end
  
  def set_transfer
    if User.find_by(:email => user_params[:email])
      receptor = User.find_by(:email => user_params[:email])
    
      temp = @user.room_id
      @user.room_id = nil
      receptor.room_id = temp
      
      @user.save
      receptor.save
      
      transfer = UserTransferUser.new
      transfer.update_attributes(:transfer => @user, :receptor => receptor)
    end
    
    redirect_to user_path(@user)
  end
  
  def history_transfer
    @transfers = UserTransferUser.all
    @transfers = UserTransferUser.paginate(:page => params[:page], :per_page => 5)
  end
  
  def password_recovery
  end
  
  def set_new_password
    if user = User.find_by(email: params[:session][:email])
      user = User.find_by(email: params[:session][:email])
      new_password = user.password_digest[0..5]
      
      user.password = new_password
      user.password_confirmation = new_password
      user.save
      
      UserMailer.password_recovery(user, new_password).deliver
    end
    
    redirect_to login_path
  end
  
  def workshops
    @users = User.paginate(:page => params[:page], :per_page => 5)
    @workshops = []
    inscriptions = UserInscriptionWorkshop.where(:user_id => @user.id) 
    
    inscriptions.each do |inscription|
      @workshops.append(Workshop.find(inscription.workshop_id))
    end
    
    respond_to do |format|
      format.html
      format.xls
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:name, :email, :telephone, :password, :password_confirmation, :profile_photo, :ej_id, :room_id)
    end
    
    def correct_user
        if current_user != @user
            redirect_to user_path(current_user)
        end
    end
    
    def correct_user_or_admin
        if current_user != @user && !current_user.admin
            redirect_to user_path(current_user)
        end
    end
    
    def set_ejs
      @ejs = Ej.all
    end
    
    def set_rooms
      @rooms = Room.all
    end
    
    def full
      @room = Room.find(params[:room_id])
      
      
      if @room.users.count == @room.capacity
      
        flash[:notice] = "Quarto cheio!"
        redirect_to local_path(@user)
      end
    end
end
