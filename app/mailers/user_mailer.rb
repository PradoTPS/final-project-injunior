class UserMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.confirmation.subject
  #
  def confirmation(user)
    @greeting =  "Olá" + " " + user.name + ","

    mail to: user.email, subject: "Confirmation"
  end
  
  def password_recovery(user, new_password)
    @greeting =  "Olá" + " " + user.name + ","
    @new_password = new_password
    
    mail to: user.email, subject: "Password Recovery"
  end
end
