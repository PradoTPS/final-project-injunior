class Speaker < ApplicationRecord
  has_many :workshops
  mount_uploader :profile_photo, ImageUploader
  
  def picture_size
        if profile_photo.size > 2.megabytes
            error.ad(:profile_photo, "Impossível carregar fotos com mais de 2MB")    
        end
    end
end
