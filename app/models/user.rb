class User < ApplicationRecord
    belongs_to :ej
    belongs_to :room, optional: true
    
    has_secure_password
    mount_uploader :profile_photo, ImageUploader
    
    VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
    validates :email, length: { in: 10..50}, format: { with: VALID_EMAIL_REGEX }, uniqueness: true
 
    validates :name, length: { in: 2..50} 
    validates :password, length: { in: 4..16, presence: true, allow_nil: true} 
    validate :picture_size
    
    #valida o tamanho da foto
    def picture_size
        if profile_photo.size > 2.megabytes
            error.ad(:profile_photo, "Impossível carregar fotos com mais de 2MB")    
        end
    end
end