class UserTransferUser < ApplicationRecord
  belongs_to :transfer, class_name: "User", foreign_key: "user1"
  belongs_to :receptor, class_name: "User", foreign_key: "user2"
end
