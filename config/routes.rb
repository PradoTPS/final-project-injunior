Rails.application.routes.draw do
  get 'users/passwordrecovery', to: 'users#password_recovery', as: :password_recovery
  post 'users/passwordrecovery', to: 'users#set_new_password'
  
  get 'transfers', to: 'users#history_transfer', as: :history_transfer
  
  get 'users/:id/transfer', to: 'users#transfer', as: :transfer
  patch 'set_transfer' => 'users#set_transfer'
  
  get 'users/:id/local', to: 'users#local', as: :local
  post 'users/:id/local', to: 'users#set_local'
  
  get 'users/:id/workshops', to: 'users#workshops', as: :workshop_user
  
  get 'users/:id/register', to: 'users#register', as: :register
  post 'users/:id/register', to: 'users#set_register', as: :set_register
  
  #Rotas de SessionsController
  get 'login', to: 'sessions#new', as: :login
  post 'login', to: 'sessions#create'
  delete 'logout', to: 'sessions#destroy', as: :logout
  
  resources :speakers
  resources :hotels
  resources :rooms
  resources :workshops
  resources :federations
  resources :ejs
  resources :users
  
  root 'sessions#new'
end